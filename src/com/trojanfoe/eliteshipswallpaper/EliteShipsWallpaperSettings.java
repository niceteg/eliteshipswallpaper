/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.eliteshipswallpaper;

import android.content.*;
import android.os.*;
import android.preference.*;
import android.util.*;

public class EliteShipsWallpaperSettings extends PreferenceActivity
    implements SharedPreferences.OnSharedPreferenceChangeListener
{
    private static final String TAG = "EliteShipsWallpaperSettings";

    @Override
    protected void onCreate(Bundle icicle)
    {
        super.onCreate(icicle);
        getPreferenceManager().setSharedPreferencesName(EliteShipsWallpaperService.SHARED_PREFS_NAME);
        addPreferencesFromResource(R.xml.settings);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        Log.d(TAG, "onSharedPreferenceChanged(key='" + key + "')");
    }
}

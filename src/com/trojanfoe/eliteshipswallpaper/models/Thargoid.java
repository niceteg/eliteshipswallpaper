/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Thargoid extends Model
{
    public Thargoid()
    {
        super("Thargoid");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.593982f,
                0.165352f, 0.495380f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.073511f, 0.763111f,
                0.235594f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.753715f,
                0.386868f, 0.617622f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.575190f,
                0.075675f, 0.548284f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 3, 2, 0, 0, 0, mMaterials[1]), new Face(0, 2, 1, 0, 0, 0, mMaterials[1]),
            new Face(4, 7, 6, 0, 0, 0, mMaterials[1]), new Face(4, 6, 5, 0, 0, 0, mMaterials[1]),
            new Face(8, 9, 10, 1, 1, 1, mMaterials[3]), new Face(8, 10, 11, 1, 1, 1, mMaterials[3]),
            new Face(8, 11, 12, 1, 1, 1, mMaterials[3]), new Face(8, 12, 13, 1, 1, 1, mMaterials[3]),
            new Face(8, 13, 14, 1, 1, 1, mMaterials[3]), new Face(8, 14, 15, 1, 1, 1, mMaterials[3]),
            new Face(16, 17, 9, 2, 2, 2, mMaterials[0]), new Face(9, 8, 16, 3, 3, 3, mMaterials[0]),
            new Face(8, 15, 18, 4, 4, 4, mMaterials[2]), new Face(8, 18, 16, 5, 5, 5, mMaterials[2]),
            new Face(17, 19, 10, 6, 6, 6, mMaterials[2]), new Face(17, 10, 9, 7, 7, 7, mMaterials[2]),
            new Face(19, 20, 11, 8, 8, 8, mMaterials[0]), new Face(19, 11, 10, 9, 9, 9, mMaterials[0]),
            new Face(20, 21, 12, 10, 10, 10, mMaterials[2]), new Face(20, 12, 11, 11, 11, 11, mMaterials[2]),
            new Face(21, 22, 13, 12, 12, 12, mMaterials[0]), new Face(21, 13, 12, 13, 13, 13, mMaterials[0]),
            new Face(22, 23, 14, 14, 14, 14, mMaterials[2]), new Face(22, 14, 13, 15, 15, 15, mMaterials[2]),
            new Face(23, 18, 15, 16, 16, 16, mMaterials[0]), new Face(23, 15, 14, 17, 17, 17, mMaterials[0]),
            new Face(18, 23, 22, 0, 0, 0, mMaterials[3]), new Face(18, 22, 21, 0, 0, 0, mMaterials[3]),
            new Face(18, 21, 20, 0, 0, 0, mMaterials[3]), new Face(18, 20, 19, 0, 0, 0, mMaterials[3]),
            new Face(18, 19, 17, 0, 0, 0, mMaterials[3]), new Face(18, 17, 16, 0, 0, 0, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(7.6729912195122f, -2.37816975609756f, 6.14828487804878f),
            new Vector3f(7.6729912195122f, -2.37816975609756f, 6.06424975609756f),
            new Vector3f(-7.93676487804878f, -2.37816975609756f, 6.06424975609756f),
            new Vector3f(-7.93676487804878f, -2.37816975609756f, 6.14828487804878f),
            new Vector3f(7.15327219512195f, -2.36576780487805f, -5.51598048780488f),
            new Vector3f(7.15327219512195f, -2.36576780487805f, -5.6000156097561f),
            new Vector3f(-8.45648390243903f, -2.36576780487805f, -5.6000156097561f),
            new Vector3f(-8.45647609756097f, -2.36576780487805f, -5.51598048780488f),
            new Vector3f(-4.68292682926829f, 3.1219512195122f, 4.68292682926829f),
            new Vector3f(0f, 3.1219512195122f, 6.63414634146342f),
            new Vector3f(4.68292682926829f, 3.1219512195122f, 4.68292682926829f),
            new Vector3f(6.63414634146342f, 3.1219512195122f, -0f),
            new Vector3f(4.68292682926829f, 3.1219512195122f, -4.68292682926829f),
            new Vector3f(0f, 3.1219512195122f, -6.63414634146342f),
            new Vector3f(-4.68292682926829f, 3.1219512195122f, -4.68292682926829f),
            new Vector3f(-6.63414634146342f, 3.1219512195122f, -0f),
            new Vector3f(-11.3170731707317f, -2.34146341463415f, 11.3170731707317f),
            new Vector3f(-0f, -2.34146341463415f, 16f), new Vector3f(-16f, -2.34146341463415f, 0f),
            new Vector3f(11.3170731707317f, -2.34146341463415f, 11.3170731707317f),
            new Vector3f(16f, -2.34146341463415f, 0f),
            new Vector3f(11.3170731707317f, -2.34146341463415f, -11.3170731707317f),
            new Vector3f(-0f, -2.34146341463415f, -16f),
            new Vector3f(-11.3170731707317f, -2.34146341463415f, -11.3170731707317f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, -1f, 0f), new Vector3f(0f, 1f, 0f), new Vector3f(-0.204109f, 0.845595f, 0.493264f),
            new Vector3f(-0.204958f, 0.846184f, 0.491899f), new Vector3f(-0.49312f, 0.845349f, 0.205467f),
            new Vector3f(-0.492757f, 0.845941f, 0.203899f), new Vector3f(0.2039f, 0.84594f, 0.492757f),
            new Vector3f(0.205467f, 0.845349f, 0.49312f), new Vector3f(0.493264f, 0.845595f, 0.204109f),
            new Vector3f(0.491899f, 0.846184f, 0.204958f), new Vector3f(0.492757f, 0.84594f, -0.2039f),
            new Vector3f(0.493121f, 0.845349f, -0.205467f), new Vector3f(0.204109f, 0.845595f, -0.493264f),
            new Vector3f(0.204958f, 0.846184f, -0.491899f), new Vector3f(-0.203899f, 0.84594f, -0.492757f),
            new Vector3f(-0.205467f, 0.845349f, -0.493121f), new Vector3f(-0.493264f, 0.845595f, -0.204109f),
            new Vector3f(-0.491899f, 0.846184f, -0.204958f)
        };
        return narr;
    }
}

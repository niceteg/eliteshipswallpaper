/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Cobramk1 extends Model
{
    public Cobramk1()
    {
        super("Cobramk1");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.320000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[1]), new Face(3, 4, 1, 1, 1, 1, mMaterials[1]),
            new Face(1, 0, 3, 2, 2, 2, mMaterials[1]), new Face(0, 2, 5, 3, 3, 3, mMaterials[2]),
            new Face(0, 5, 6, 3, 3, 3, mMaterials[2]), new Face(3, 0, 6, 4, 4, 4, mMaterials[4]),
            new Face(6, 5, 7, 5, 5, 5, mMaterials[1]), new Face(1, 4, 2, 6, 6, 6, mMaterials[2]),
            new Face(8, 3, 6, 7, 7, 7, mMaterials[1]), new Face(8, 6, 7, 8, 8, 8, mMaterials[1]),
            new Face(4, 8, 5, 9, 9, 9, mMaterials[0]), new Face(4, 5, 2, 9, 9, 9, mMaterials[0]),
            new Face(3, 8, 4, 10, 10, 10, mMaterials[2]), new Face(8, 7, 5, 11, 11, 11, mMaterials[2]),
            new Face(9, 10, 11, 12, 12, 12, mMaterials[3]), new Face(9, 11, 12, 12, 12, 12, mMaterials[3]),
            new Face(11, 10, 13, 10, 10, 10, mMaterials[3]), new Face(11, 13, 14, 10, 10, 10, mMaterials[3]),
            new Face(14, 13, 15, 13, 13, 13, mMaterials[3]), new Face(14, 15, 16, 13, 13, 13, mMaterials[3]),
            new Face(16, 15, 9, 14, 14, 14, mMaterials[3]), new Face(16, 9, 12, 14, 14, 14, mMaterials[3]),
            new Face(9, 15, 13, 15, 15, 15, mMaterials[3]), new Face(9, 13, 10, 15, 15, 15, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(-5.87755102040816f, -0.339591836734694f, 16.3265306122449f),
            new Vector3f(-21.5510204081633f, 0f, 2.29877551020408f),
            new Vector3f(-17.6326530612245f, -3.91836734693878f, -12.4081632653061f),
            new Vector3f(0f, 3.91836734693878f, -1.95918367346939f),
            new Vector3f(-10.4489795918367f, 3.91836734693878f, -12.4081632653061f),
            new Vector3f(17.6326530612245f, -3.91836734693878f, -12.4081632653061f),
            new Vector3f(5.87755102040816f, -0.339591836734694f, 16.3265306122449f),
            new Vector3f(21.5510204081633f, 0f, 2.29877551020408f),
            new Vector3f(10.4489795918367f, 3.91836734693878f, -12.4081632653061f),
            new Vector3f(-0.261224489795918f, -0.600816326530612f, 19.5918367346939f),
            new Vector3f(-0.261224489795918f, -0.0783673469387755f, 19.5918367346939f),
            new Vector3f(-0.261224489795918f, -0.0783673469387755f, 16.3265306122449f),
            new Vector3f(-0.261224489795918f, -0.600816326530612f, 16.3265306122449f),
            new Vector3f(0.261224489795918f, -0.0783673469387755f, 19.5918367346939f),
            new Vector3f(0.261224489795918f, -0.0783673469387755f, 16.3265306122449f),
            new Vector3f(0.261224489795918f, -0.600816326530612f, 19.5918367346939f),
            new Vector3f(0.261224489795918f, -0.600816326530612f, 16.3265306122449f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0.201326f, -0.958524f, 0.20174f), new Vector3f(-0.148439f, 0.977718f, 0.148439f),
            new Vector3f(-0.141142f, 0.973254f, 0.181262f), new Vector3f(0f, -0.992333f, 0.123591f),
            new Vector3f(0f, 0.973944f, 0.22679f), new Vector3f(0.201326f, -0.958524f, 0.20174f),
            new Vector3f(-0.689893f, 0.632402f, -0.352299f), new Vector3f(0.170987f, 0.970323f, 0.170987f),
            new Vector3f(0.126548f, 0.978129f, 0.165074f), new Vector3f(0f, 0f, -1f), new Vector3f(0f, 1f, -0f),
            new Vector3f(0.689893f, 0.632402f, -0.352299f), new Vector3f(-1f, 0f, 0f), new Vector3f(1f, -0f, 0f),
            new Vector3f(0f, -1f, 0f), new Vector3f(0f, 0f, 1f)
        };
        return narr;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Gecko extends Model
{
    public Gecko()
    {
        super("Gecko");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.640000f, 0.640000f,
                0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.024634f,
                0.255723f, 0.041692f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.055243f, 0.758413f,
                0.170963f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[0]), new Face(0, 2, 3, 0, 0, 0, mMaterials[0]),
            new Face(0, 3, 4, 1, 1, 1, mMaterials[2]), new Face(4, 3, 5, 2, 2, 2, mMaterials[2]),
            new Face(3, 2, 6, 3, 3, 3, mMaterials[0]), new Face(3, 6, 5, 3, 3, 3, mMaterials[0]),
            new Face(7, 6, 2, 4, 4, 4, mMaterials[2]), new Face(2, 1, 7, 5, 5, 5, mMaterials[2]),
            new Face(7, 1, 6, 6, 6, 6, mMaterials[1]), new Face(1, 0, 5, 7, 7, 7, mMaterials[3]),
            new Face(1, 5, 6, 7, 7, 7, mMaterials[3]), new Face(5, 0, 4, 8, 8, 8, mMaterials[1])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(3.87878787878788f, 1.93939393939394f, -5.57575757575758f),
            new Vector3f(-3.87878787878788f, 1.93939393939394f, -5.57575757575758f),
            new Vector3f(-2.42424242424242f, -0.96969696969697f, 11.3939393939394f),
            new Vector3f(2.42424242424242f, -0.96969696969697f, 11.3939393939394f),
            new Vector3f(16f, 0f, -0.727272727272727f),
            new Vector3f(4.84848484848485f, -3.39393939393939f, -5.57575757575758f),
            new Vector3f(-4.84848484848485f, -3.39393939393939f, -5.57575757575758f),
            new Vector3f(-16f, 0f, -0.727272727272727f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0f, 0.985622f, 0.168964f), new Vector3f(0.086687f, 0.980648f, 0.175541f),
            new Vector3f(0.219177f, -0.961008f, 0.168598f), new Vector3f(0f, -0.989949f, 0.141421f),
            new Vector3f(-0.219177f, -0.961008f, 0.168598f), new Vector3f(-0.086687f, 0.980648f, 0.175541f),
            new Vector3f(-0.380014f, 0.069093f, -0.922397f), new Vector3f(0f, 0f, -1f),
            new Vector3f(0.380014f, 0.069093f, -0.922397f)
        };
        return narr;
    }
}

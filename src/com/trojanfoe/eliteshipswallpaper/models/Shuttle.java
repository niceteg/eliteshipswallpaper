/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Shuttle extends Model
{
    public Shuttle()
    {
        super("Shuttle");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.178415f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.008", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.800000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.007", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.320000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[1]), new Face(0, 3, 4, 1, 1, 1, mMaterials[1]),
            new Face(3, 0, 2, 2, 2, 2, mMaterials[4]), new Face(2, 5, 3, 3, 3, 3, mMaterials[2]),
            new Face(6, 5, 2, 4, 4, 4, mMaterials[3]), new Face(6, 7, 5, 5, 5, 5, mMaterials[1]),
            new Face(8, 7, 6, 6, 6, 6, mMaterials[3]), new Face(8, 4, 7, 7, 7, 7, mMaterials[2]),
            new Face(0, 4, 8, 8, 8, 8, mMaterials[4]), new Face(4, 3, 5, 9, 9, 9, mMaterials[2]),
            new Face(4, 5, 7, 9, 9, 9, mMaterials[2]), new Face(2, 1, 6, 10, 10, 10, mMaterials[1]),
            new Face(6, 1, 8, 11, 11, 11, mMaterials[1]), new Face(1, 0, 8, 12, 12, 12, mMaterials[1]),
            new Face(9, 10, 11, 9, 9, 9, mMaterials[0]), new Face(9, 11, 12, 9, 9, 9, mMaterials[0]),
            new Face(13, 14, 15, 13, 13, 13, mMaterials[2]), new Face(16, 17, 18, 14, 14, 14, mMaterials[2])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(0f, -10.4595270140358f, 14.1511247836954f),
            new Vector3f(0f, -5.53739665448952f, 21.5343203230148f),
            new Vector3f(-10.4595270140358f, 0f, 14.1511247836954f),
            new Vector3f(-12.3053258988656f, -12.3053258988656f, -16.6121899634686f),
            new Vector3f(12.3053258988656f, -12.3053258988656f, -16.6121899634686f),
            new Vector3f(-12.3053258988656f, 12.3053258988656f, -16.6121899634686f),
            new Vector3f(0f, 11.074793308979f, 14.1511247836954f),
            new Vector3f(12.3053258988656f, 12.3053258988656f, -16.6121899634686f),
            new Vector3f(11.074793308979f, 0f, 14.1511247836954f),
            new Vector3f(0f, 1.84579888482984f, -16.618342626418f),
            new Vector3f(3.0763314747164f, 0f, -16.618342626418f),
            new Vector3f(0f, -1.23053258988656f, -16.618342626418f),
            new Vector3f(-3.0763314747164f, 0f, -16.618342626418f),
            new Vector3f(2.46106517977312f, 6.76792924437608f, 15.381657373582f),
            new Vector3f(1.84579888482984f, -0.61526629494328f, 19.0732551432417f),
            new Vector3f(6.76792924437608f, 2.46106517977312f, 15.381657373582f),
            new Vector3f(-6.1526629494328f, 2.46106517977312f, 15.381657373582f),
            new Vector3f(-1.84579888482984f, -0.61526629494328f, 19.0732551432417f),
            new Vector3f(-1.84579888482984f, 6.76792924437608f, 15.381657373582f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0.639602f, -0.639602f, 0.426401f), new Vector3f(0f, -0.998205f, 0.059892f),
            new Vector3f(-0.672429f, -0.672429f, 0.309318f), new Vector3f(-0.998205f, 0f, 0.059892f),
            new Vector3f(-0.692772f, 0.654285f, 0.30328f), new Vector3f(-0f, 0.999201f, 0.039968f),
            new Vector3f(0.675183f, 0.675183f, 0.29708f), new Vector3f(0.999201f, 0f, 0.039968f),
            new Vector3f(0.654285f, -0.692772f, 0.30328f), new Vector3f(0f, 0f, -1f),
            new Vector3f(-0.39505f, 0.373103f, 0.839482f), new Vector3f(0.376288f, 0.376288f, 0.846649f),
            new Vector3f(0.617876f, -0.654221f, 0.436147f), new Vector3f(0.386494f, 0.386494f, 0.837404f),
            new Vector3f(-0.408248f, 0.408248f, 0.816496f)
        };
        return narr;
    }
}

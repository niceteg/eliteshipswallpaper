/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Mamba extends Model
{
    public Mamba()
    {
        super("Mamba");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.791299f,
                0.587390f, 0.737207f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.772507f,
                0.342074f, 0.718990f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.243074f,
                0.048247f, 0.270225f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.777205f,
                0.207229f, 0.626005f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[5]), new Face(3, 2, 4, 1, 1, 1, mMaterials[1]),
            new Face(0, 2, 3, 2, 2, 2, mMaterials[2]), new Face(4, 2, 1, 3, 3, 3, mMaterials[2]),
            new Face(0, 3, 4, 4, 4, 4, mMaterials[0]), new Face(0, 4, 1, 4, 4, 4, mMaterials[0]),
            new Face(5, 6, 7, 0, 0, 0, mMaterials[3]), new Face(8, 9, 10, 4, 4, 4, mMaterials[3]),
            new Face(8, 10, 11, 4, 4, 4, mMaterials[3]), new Face(12, 13, 14, 1, 1, 1, mMaterials[4]),
            new Face(12, 14, 15, 1, 1, 1, mMaterials[4]), new Face(16, 17, 18, 0, 0, 0, mMaterials[3]),
            new Face(19, 20, 21, 4, 4, 4, mMaterials[3]), new Face(22, 23, 24, 4, 4, 4, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(-21.3311113425685f, -2.66638891782106f, -10.6655556712842f),
            new Vector3f(21.3311113425685f, -2.66638891782106f, -10.6655556712842f),
            new Vector3f(0f, 0f, 21.3311113425685f),
            new Vector3f(-10.6655556712842f, 2.66638891782106f, -10.6655556712842f),
            new Vector3f(10.6655556712842f, 2.66638891782106f, -10.6655556712842f),
            new Vector3f(-7.99916675346318f, -2.46950942610145f, -6.66597229455265f),
            new Vector3f(-5.33277783564212f, -2.46950942610145f, -6.66597229455265f),
            new Vector3f(-6.66597229455265f, -1.46961358191855f, 5.33277783564212f),
            new Vector3f(2.66638891782106f, -1.33319445891053f, -10.6688886574315f),
            new Vector3f(-2.66638891782106f, -1.33319445891053f, -10.6688886574315f),
            new Vector3f(-2.66638891782106f, 1.33319445891053f, -10.6688886574315f),
            new Vector3f(2.66638891782106f, 1.33319445891053f, -10.6688886574315f),
            new Vector3f(2.66638891782106f, 1.00322883033017f, 9.33236121237371f),
            new Vector3f(1.33319445891053f, 1.33652744505781f, 5.33277783564212f),
            new Vector3f(-1.33319445891053f, 1.33652744505781f, 5.33277783564212f),
            new Vector3f(-2.66638891782106f, 1.00322883033017f, 9.33236121237371f),
            new Vector3f(5.33277783564212f, -2.45117800229143f, -6.66597229455265f),
            new Vector3f(7.99916675346318f, -2.45117800229143f, -6.66597229455265f),
            new Vector3f(6.66597229455265f, -1.45128215810853f, 5.33277783564212f),
            new Vector3f(10.6655556712842f, 1.33319445891053f, -10.6688886574315f),
            new Vector3f(12.66534735965f, 0f, -10.6688886574315f),
            new Vector3f(11.9987501301948f, -1.33319445891053f, -10.6688886574315f),
            new Vector3f(-11.9987501301948f, -1.33319445891053f, -10.6688886574315f),
            new Vector3f(-12.66534735965f, 0f, -10.6688886574315f),
            new Vector3f(-10.6655556712842f, 1.33319445891053f, -10.6688886574315f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, -0.996546f, 0.083045f), new Vector3f(0f, 0.996546f, 0.083045f),
            new Vector3f(-0.436436f, 0.872872f, 0.218218f), new Vector3f(0.436436f, 0.872872f, 0.218218f),
            new Vector3f(-0f, 0f, -1f)
        };
        return narr;
    }
}

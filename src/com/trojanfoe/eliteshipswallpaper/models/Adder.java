/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Adder extends Model
{
    public Adder()
    {
        super("Adder");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f, 0.160000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.640000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.320000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[1]), new Face(0, 2, 3, 0, 0, 0, mMaterials[1]),
            new Face(3, 2, 4, 1, 1, 1, mMaterials[1]), new Face(3, 4, 5, 1, 1, 1, mMaterials[1]),
            new Face(6, 0, 3, 2, 2, 2, mMaterials[3]), new Face(3, 5, 6, 3, 3, 3, mMaterials[0]),
            new Face(7, 0, 6, 4, 4, 4, mMaterials[2]), new Face(7, 6, 8, 4, 4, 4, mMaterials[2]),
            new Face(8, 6, 5, 5, 5, 5, mMaterials[2]), new Face(8, 5, 9, 5, 5, 5, mMaterials[2]),
            new Face(8, 9, 10, 6, 6, 6, mMaterials[4]), new Face(8, 10, 11, 6, 6, 6, mMaterials[4]),
            new Face(8, 11, 12, 6, 6, 6, mMaterials[4]), new Face(8, 12, 7, 6, 6, 6, mMaterials[4]),
            new Face(1, 12, 11, 7, 7, 7, mMaterials[2]), new Face(1, 11, 13, 7, 7, 7, mMaterials[2]),
            new Face(13, 11, 10, 8, 8, 8, mMaterials[2]), new Face(13, 10, 4, 8, 8, 8, mMaterials[2]),
            new Face(1, 13, 2, 9, 9, 9, mMaterials[3]), new Face(2, 13, 4, 10, 10, 10, mMaterials[0]),
            new Face(1, 0, 7, 11, 11, 11, mMaterials[5]), new Face(1, 7, 12, 11, 11, 11, mMaterials[5]),
            new Face(10, 9, 5, 12, 12, 12, mMaterials[3]), new Face(10, 5, 4, 12, 12, 12, mMaterials[3]),
            new Face(14, 15, 16, 13, 13, 13, mMaterials[6]), new Face(14, 16, 17, 13, 13, 13, mMaterials[6])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(7.25292857142857f, 2.4432380952381f, 5.7232380952381f),
            new Vector3f(-7.14707142857143f, 2.4432380952381f, 5.7232380952381f),
            new Vector3f(-7.14707142857143f, -0.356761904761905f, 16.5232380952381f),
            new Vector3f(7.25292857142857f, -0.356761904761905f, 16.5232380952381f),
            new Vector3f(-7.14707142857143f, -3.1567619047619f, 5.7232380952381f),
            new Vector3f(7.25292857142857f, -3.1567619047619f, 5.7232380952381f),
            new Vector3f(12.0529285714286f, -0.356761904761905f, -9.07676190476191f),
            new Vector3f(7.25292857142857f, 2.4432380952381f, -15.4767619047619f),
            new Vector3f(12.0529285714286f, -0.356761904761905f, -15.4767619047619f),
            new Vector3f(7.25292857142857f, -3.1567619047619f, -15.4767619047619f),
            new Vector3f(-7.14707142857143f, -3.1567619047619f, -15.4767619047619f),
            new Vector3f(-11.9470714285714f, -0.356761904761905f, -15.4767619047619f),
            new Vector3f(-7.14707142857143f, 2.4432380952381f, -15.4767619047619f),
            new Vector3f(-11.9470714285714f, -0.356761904761905f, -9.07676190476191f),
            new Vector3f(-4.34707142857143f, 1.296f, 10.1232380952381f),
            new Vector3f(-4.34707142857143f, 0.896f, 12.1232380952381f),
            new Vector3f(4.45292857142857f, 0.896f, 12.1232380952381f),
            new Vector3f(4.45292857142857f, 1.296f, 10.1232380952381f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0f, 0.967997f, 0.250962f), new Vector3f(0f, -0.967997f, 0.250962f),
            new Vector3f(0.801104f, 0.57937f, 0.150207f), new Vector3f(0.801104f, -0.57937f, 0.150207f),
            new Vector3f(0.503871f, 0.863779f, 0f), new Vector3f(0.503871f, -0.863779f, 0f), new Vector3f(0f, 0f, -1f),
            new Vector3f(-0.503871f, 0.863779f, 0f), new Vector3f(-0.503871f, -0.863779f, 0f),
            new Vector3f(-0.801104f, 0.57937f, 0.150207f), new Vector3f(-0.801104f, -0.57937f, 0.150207f),
            new Vector3f(0f, 1f, -0f), new Vector3f(-0f, -1f, -0f), new Vector3f(0f, 0.980581f, 0.196116f)
        };
        return narr;
    }
}

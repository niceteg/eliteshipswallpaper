/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Sidewinder extends Model
{
    public Sidewinder()
    {
        super("Sidewinder");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f, 0.000000f,
                0.320000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.160000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[2]), new Face(0, 3, 1, 1, 1, 1, mMaterials[0]),
            new Face(4, 0, 2, 2, 2, 2, mMaterials[0]), new Face(4, 5, 3, 3, 3, 3, mMaterials[1]),
            new Face(3, 0, 4, 3, 3, 3, mMaterials[1]), new Face(5, 1, 3, 4, 4, 4, mMaterials[0]),
            new Face(2, 1, 5, 5, 5, 5, mMaterials[2]), new Face(4, 2, 5, 6, 6, 6, mMaterials[0]),
            new Face(6, 7, 8, 3, 3, 3, mMaterials[3]), new Face(6, 8, 9, 3, 3, 3, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(0f, 4f, -7f), new Vector3f(-8f, 0f, 9f), new Vector3f(8f, 0f, 9f),
            new Vector3f(-16f, 0f, -7f), new Vector3f(16f, 0f, -7f), new Vector3f(0f, -4f, -7f),
            new Vector3f(3f, -1.5f, -7.0025f), new Vector3f(-3f, -1.5f, -7.0025f), new Vector3f(-3f, 1.5f, -7.0025f),
            new Vector3f(3f, 1.5f, -7.0025f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, 0.970143f, 0.242536f), new Vector3f(-0.240772f, 0.963087f, 0.120386f),
            new Vector3f(0.240772f, 0.963087f, 0.120386f), new Vector3f(0f, 0f, -1f),
            new Vector3f(-0.240772f, -0.963087f, 0.120386f), new Vector3f(0f, -0.970143f, 0.242536f),
            new Vector3f(0.240772f, -0.963087f, 0.120386f)
        };
        return narr;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Krait extends Model
{
    public Krait()
    {
        super("Krait");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("PaleGreen", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.552000f, 0.648000f,
                0.456000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Black", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f, 0.112000f,
                0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f, 0.800000f,
                0.075279f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("DarkGreen", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.315590f, 0.368476f,
                0.262704f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[3]), new Face(1, 0, 3, 1, 1, 1, mMaterials[3]),
            new Face(0, 4, 3, 2, 2, 2, mMaterials[3]), new Face(4, 0, 2, 3, 3, 3, mMaterials[3]),
            new Face(1, 3, 2, 4, 4, 4, mMaterials[3]), new Face(4, 2, 3, 5, 5, 5, mMaterials[1]),
            new Face(5, 6, 7, 6, 6, 6, mMaterials[2]), new Face(8, 9, 10, 7, 7, 7, mMaterials[2]),
            new Face(11, 12, 13, 8, 8, 8, mMaterials[0]), new Face(13, 14, 11, 9, 9, 9, mMaterials[0]),
            new Face(4, 15, 16, 10, 10, 10, mMaterials[1]), new Face(4, 16, 17, 10, 10, 10, mMaterials[1]),
            new Face(18, 17, 16, 11, 11, 11, mMaterials[1]), new Face(18, 16, 19, 11, 11, 11, mMaterials[1]),
            new Face(18, 19, 20, 12, 12, 12, mMaterials[1]), new Face(18, 20, 21, 12, 12, 12, mMaterials[1]),
            new Face(20, 15, 4, 13, 13, 13, mMaterials[1]), new Face(20, 4, 21, 13, 13, 13, mMaterials[1]),
            new Face(22, 23, 24, 14, 14, 14, mMaterials[1]), new Face(22, 24, 1, 14, 14, 14, mMaterials[1]),
            new Face(25, 1, 24, 11, 11, 11, mMaterials[1]), new Face(25, 24, 26, 11, 11, 11, mMaterials[1]),
            new Face(25, 26, 27, 12, 12, 12, mMaterials[1]), new Face(25, 27, 28, 12, 12, 12, mMaterials[2]),
            new Face(27, 23, 22, 13, 13, 13, mMaterials[1]), new Face(27, 22, 28, 13, 13, 13, mMaterials[1]),
            new Face(15, 20, 16, 15, 15, 15, mMaterials[1]), new Face(20, 19, 16, 15, 15, 15, mMaterials[1]),
            new Face(4, 17, 21, 16, 16, 16, mMaterials[1]), new Face(17, 18, 21, 16, 16, 16, mMaterials[1]),
            new Face(1, 25, 22, 16, 16, 16, mMaterials[1]), new Face(25, 28, 22, 16, 16, 16, mMaterials[1]),
            new Face(23, 27, 26, 15, 15, 15, mMaterials[1]), new Face(23, 26, 24, 15, 15, 15, mMaterials[1])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(0f, 0f, 16.695652173913f), new Vector3f(16f, 0.173913043478261f, -0.521739130434782f),
            new Vector3f(0f, 3.13043478260869f, -8.34782608695652f),
            new Vector3f(0f, -3.13043478260869f, -8.34782608695652f),
            new Vector3f(-16f, 0.173913043478261f, -0.521739130434782f),
            new Vector3f(3.13043478260869f, -1.91304347826087f, -6.9343652173913f),
            new Vector3f(3.13043478260869f, 1.91304347826087f, -6.9343652173913f),
            new Vector3f(6.26086956521739f, -0f, -5.36914782608696f),
            new Vector3f(-6.26086956521739f, -0f, -5.33286956521739f),
            new Vector3f(-3.13043478260869f, 1.91304347826087f, -6.89808695652174f),
            new Vector3f(-3.13043478260869f, -1.91304347826087f, -6.89808695652174f),
            new Vector3f(0f, 1.10187826086957f, 9.21739130434783f),
            new Vector3f(3.13043478260869f, 1.44970434782609f, 3.30434782608696f),
            new Vector3f(0f, 1.43231304347826f, 6.60869565217391f),
            new Vector3f(-3.13043478260869f, 1.44970434782609f, 3.30434782608696f),
            new Vector3f(-16f, 0.173913043478261f, 15.1304347826087f),
            new Vector3f(-15.6521739130435f, 0.0173913043478261f, 15.1304347826087f),
            new Vector3f(-15.6521739130435f, 0.0173913043478261f, -0.521739130434782f),
            new Vector3f(-15.6521739130435f, -0.173913043478261f, -0.521739130434782f),
            new Vector3f(-15.6521739130435f, -0.173913043478261f, 15.1304347826087f),
            new Vector3f(-16f, -0.173913043478261f, 15.1304347826087f),
            new Vector3f(-16f, -0.173913043478261f, -0.521739130434782f),
            new Vector3f(15.6521739130435f, 0.0173913043478261f, -0.521739130434782f),
            new Vector3f(15.6521739130435f, 0.0173913043478261f, 15.1304347826087f),
            new Vector3f(16f, 0.173913043478261f, 15.1304347826087f),
            new Vector3f(16f, -0.173913043478261f, -0.521739130434782f),
            new Vector3f(16f, -0.173913043478261f, 15.1304347826087f),
            new Vector3f(15.6521739130435f, -0.173913043478261f, 15.1304347826087f),
            new Vector3f(15.6521739130435f, -0.173913043478261f, -0.521739130434782f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0.121773f, 0.984893f, 0.123112f), new Vector3f(0.14278f, -0.982112f, 0.122764f),
            new Vector3f(-0.14278f, -0.982112f, 0.122764f), new Vector3f(-0.121773f, 0.984893f, 0.123112f),
            new Vector3f(0.439385f, 0f, -0.898299f), new Vector3f(-0.439385f, 0f, -0.898299f),
            new Vector3f(0.447214f, 0f, -0.894427f), new Vector3f(-0.447214f, 0f, -0.894427f),
            new Vector3f(0.126117f, 0.984152f, 0.124659f), new Vector3f(-0.126117f, 0.984152f, 0.124659f),
            new Vector3f(0.410365f, 0.911921f, 0f), new Vector3f(1f, 0f, 0f), new Vector3f(0f, -1f, 0f),
            new Vector3f(-1f, 0f, 0f), new Vector3f(-0.410365f, 0.911921f, 0f), new Vector3f(0f, -0f, 1f),
            new Vector3f(0f, 0f, -1f)
        };
        return narr;
    }
}

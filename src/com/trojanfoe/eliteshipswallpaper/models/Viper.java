/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Viper extends Model
{
    public Viper()
    {
        super("Viper");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.640000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.480000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.005", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.320000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.006", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.000000f, 0.160000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[5]), new Face(1, 3, 4, 1, 1, 1, mMaterials[2]),
            new Face(1, 4, 2, 1, 1, 1, mMaterials[2]), new Face(5, 0, 2, 2, 2, 2, mMaterials[2]),
            new Face(5, 2, 4, 2, 2, 2, mMaterials[2]), new Face(6, 4, 3, 3, 3, 3, mMaterials[1]),
            new Face(6, 3, 7, 3, 3, 3, mMaterials[1]), new Face(4, 6, 8, 4, 4, 4, mMaterials[1]),
            new Face(4, 8, 5, 4, 4, 4, mMaterials[1]), new Face(6, 7, 8, 5, 5, 5, mMaterials[0]),
            new Face(1, 0, 3, 6, 6, 6, mMaterials[4]), new Face(3, 0, 5, 6, 6, 6, mMaterials[4]),
            new Face(3, 5, 8, 6, 6, 6, mMaterials[4]), new Face(3, 8, 7, 6, 6, 6, mMaterials[4]),
            new Face(9, 10, 11, 6, 6, 6, mMaterials[3]), new Face(12, 13, 14, 6, 6, 6, mMaterials[3])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(7.98298294136333f, 5.32198862757555f, -15.8038048892444f),
            new Vector3f(-7.98298294136333f, 5.32198862757555f, -15.8038048892444f),
            new Vector3f(0f, 5.32198862757555f, 0.162160993482227f),
            new Vector3f(-15.9659658827267f, -0f, -15.8038048892444f), new Vector3f(0f, -0f, 16.1281268762089f),
            new Vector3f(15.9659658827267f, -0f, -15.8038048892444f),
            new Vector3f(0f, -5.32198862757555f, 0.162160993482227f),
            new Vector3f(-7.98298294136333f, -5.32198862757555f, -15.8038048892444f),
            new Vector3f(7.98298294136333f, -5.32198862757555f, -15.8038048892444f),
            new Vector3f(-2.66099431378778f, 2.84651883734506f, -15.8578762937006f),
            new Vector3f(-2.66099431378778f, -2.47546979023049f, -15.8578762937006f),
            new Vector3f(-10.6439772551511f, 0.185524523557284f, -15.8578762937006f),
            new Vector3f(10.6439772551511f, -0f, -15.8718731237911f),
            new Vector3f(2.66099431378778f, -2.66099431378778f, -15.8718731237911f),
            new Vector3f(2.66099431378778f, 2.66099431378778f, -15.8718731237911f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(-0f, 1f, 0f), new Vector3f(-0.534522f, 0.801784f, 0.267261f),
            new Vector3f(0.534522f, 0.801784f, 0.267261f), new Vector3f(-0.534522f, -0.801784f, 0.267261f),
            new Vector3f(0.534522f, -0.801784f, 0.267261f), new Vector3f(0f, -1f, 0f), new Vector3f(0f, 0f, -1f)
        };
        return narr;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.eliteshipswallpaper.models;

import com.trojanfoe.opengl.Face;
import com.trojanfoe.opengl.Material;
import com.trojanfoe.opengl.Model;
import com.trojanfoe.opengl.Vector3f;

public final class Asp extends Model
{
    public Asp()
    {
        super("Asp");
        createMaterials();
        createBuffers(createFaces(), createVertices(), createNormals());
    }

    protected void createMaterials()
    {
        mMaterials = new Material[]
        {
            new Material("Material", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f, 0.320000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.003", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.800000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.004", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.112000f,
                0.112000f, 0.112000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.001", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Thruster", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.800000f, 0.800000f,
                0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f),
            new Material("Material.002", new Vector3f(0.000000f, 0.000000f, 0.000000f), new Vector3f(0.000000f,
                0.480000f, 0.000000f), new Vector3f(0.500000f, 0.500000f, 0.500000f), 12.298039168f, 1.000000f)
        };
    }

    protected Face[] createFaces()
    {
        Face[] farr = new Face[]
        {
            new Face(0, 1, 2, 0, 0, 0, mMaterials[0]), new Face(0, 2, 3, 1, 1, 1, mMaterials[0]),
            new Face(0, 3, 4, 2, 2, 2, mMaterials[0]), new Face(5, 4, 3, 3, 3, 3, mMaterials[3]),
            new Face(5, 3, 6, 4, 4, 4, mMaterials[3]), new Face(5, 6, 7, 5, 5, 5, mMaterials[3]),
            new Face(6, 3, 2, 6, 6, 6, mMaterials[3]), new Face(6, 2, 8, 7, 7, 7, mMaterials[3]),
            new Face(6, 8, 9, 8, 8, 8, mMaterials[3]), new Face(10, 11, 12, 9, 9, 9, mMaterials[5]),
            new Face(1, 0, 11, 10, 10, 10, mMaterials[2]), new Face(1, 11, 10, 10, 10, 10, mMaterials[2]),
            new Face(7, 12, 11, 11, 11, 11, mMaterials[0]), new Face(7, 11, 5, 12, 12, 12, mMaterials[5]),
            new Face(0, 4, 5, 13, 13, 13, mMaterials[5]), new Face(8, 2, 1, 14, 14, 14, mMaterials[5]),
            new Face(11, 0, 5, 15, 15, 15, mMaterials[1]), new Face(1, 10, 8, 16, 16, 16, mMaterials[1]),
            new Face(7, 6, 9, 17, 17, 17, mMaterials[2]), new Face(7, 9, 12, 17, 17, 17, mMaterials[2]),
            new Face(13, 14, 15, 17, 17, 17, mMaterials[4]), new Face(13, 15, 16, 17, 17, 17, mMaterials[4]),
            new Face(17, 18, 19, 18, 18, 18, mMaterials[2]), new Face(17, 19, 20, 18, 18, 18, mMaterials[2]),
            new Face(21, 17, 20, 19, 19, 19, mMaterials[2]), new Face(21, 20, 22, 19, 19, 19, mMaterials[2]),
            new Face(23, 24, 21, 20, 20, 20, mMaterials[2]), new Face(23, 21, 22, 20, 20, 20, mMaterials[2]),
            new Face(18, 24, 23, 21, 21, 21, mMaterials[2]), new Face(18, 23, 19, 21, 21, 21, mMaterials[2]),
            new Face(9, 10, 12, 22, 22, 22, mMaterials[0]), new Face(8, 10, 9, 23, 23, 23, mMaterials[5]),
            new Face(17, 21, 24, 24, 24, 24, mMaterials[2]), new Face(17, 24, 18, 24, 24, 24, mMaterials[2])
        };
        return farr;
    }

    protected Vector3f[] createVertices()
    {
        Vector3f[] varr = new Vector3f[]
        {
            new Vector3f(6.02898550724638f, -1.6231884057971f, 16.9275362318841f),
            new Vector3f(-6.02898550724638f, -1.6231884057971f, 16.9275362318841f),
            new Vector3f(-9.97101449275362f, -3.2463768115942f, 6.49275362318841f),
            new Vector3f(0f, -4.17391304347826f, 0f),
            new Vector3f(9.97101449275362f, -3.2463768115942f, 6.49275362318841f),
            new Vector3f(16f, -0.695652173913043f, 0f), new Vector3f(0f, -2.08695652173913f, -10.4347826086957f),
            new Vector3f(9.97101449275362f, 0f, -10.4347826086957f), new Vector3f(-16f, -0.695652173913043f, 0f),
            new Vector3f(-9.97101449275362f, 0f, -10.4347826086957f),
            new Vector3f(-9.97101449275362f, 3.2463768115942f, 6.49275362318841f),
            new Vector3f(9.97101449275362f, 3.2463768115942f, 6.49275362318841f),
            new Vector3f(0f, 2.08695652173913f, -10.4347826086957f),
            new Vector3f(3.94202898550725f, 0f, -10.4371014492754f),
            new Vector3f(0f, -0.927536231884058f, -10.4371014492754f),
            new Vector3f(-3.94202898550725f, 0f, -10.4371014492754f),
            new Vector3f(0f, 0.927536231884058f, -10.4371014492754f),
            new Vector3f(-0.231884057971015f, -1.85507246376812f, 19.2463768115942f),
            new Vector3f(-0.231884057971015f, -1.39130434782609f, 19.2463768115942f),
            new Vector3f(-0.231884057971015f, -1.39130434782609f, 16.9275362318841f),
            new Vector3f(-0.231884057971015f, -1.85507246376812f, 16.9275362318841f),
            new Vector3f(0.231884057971015f, -1.85507246376812f, 19.2463768115942f),
            new Vector3f(0.231884057971015f, -1.85507246376812f, 16.9275362318841f),
            new Vector3f(0.231884057971015f, -1.39130434782609f, 16.9275362318841f),
            new Vector3f(0.231884057971015f, -1.39130434782609f, 19.2463768115942f)
        };
        return varr;
    }

    protected Vector3f[] createNormals()
    {
        Vector3f[] narr = new Vector3f[]
        {
            new Vector3f(0f, -0.988116f, 0.153707f), new Vector3f(0.004092f, -0.989042f, 0.147576f),
            new Vector3f(-0.00656f, -0.988469f, 0.151284f), new Vector3f(0.208824f, -0.96059f, -0.183467f),
            new Vector3f(0.208485f, -0.959033f, -0.191807f), new Vector3f(0.201495f, -0.962696f, -0.180599f),
            new Vector3f(-0.213856f, -0.957895f, -0.191579f), new Vector3f(-0.205335f, -0.960708f, -0.186753f),
            new Vector3f(-0.201495f, -0.962696f, -0.180599f), new Vector3f(0f, 0.997663f, -0.068333f),
            new Vector3f(0f, 0.906183f, 0.422886f), new Vector3f(0.201347f, 0.961989f, -0.184491f),
            new Vector3f(0.402207f, 0.899163f, -0.172442f), new Vector3f(0.660261f, -0.663128f, 0.352585f),
            new Vector3f(-0.660261f, -0.663128f, 0.352585f), new Vector3f(0.779925f, 0.40005f, 0.481328f),
            new Vector3f(-0.779925f, 0.40005f, 0.481328f), new Vector3f(0f, 0f, -1f), new Vector3f(-1f, 0f, 0f),
            new Vector3f(0f, -1f, 0f), new Vector3f(1f, -0f, 0f), new Vector3f(0f, 1f, -0f),
            new Vector3f(-0.201347f, 0.961989f, -0.184491f), new Vector3f(-0.402207f, 0.899163f, -0.172442f),
            new Vector3f(0f, 0f, 1f)
        };
        return narr;
    }
}

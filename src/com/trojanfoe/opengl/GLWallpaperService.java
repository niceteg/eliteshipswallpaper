/*
 * Copyright (c) 2011 trojanfoe apps
 */

/*
 * Taken from: http://www.rbgrn.net/content/354-glsurfaceview-adapted-3d-live-wallpapers
 */
package com.trojanfoe.opengl;

import android.service.wallpaper.*;
import android.view.*;
import com.trojanfoe.opengl.BaseConfigChooser.ComponentSizeChooser;
import com.trojanfoe.opengl.BaseConfigChooser.SimpleEGLConfigChooser;
import javax.microedition.khronos.egl.*;
import javax.microedition.khronos.opengles.*;

public class GLWallpaperService extends WallpaperService
{
    // private static final String TAG = "GLWallpaperService";

    @Override
    public Engine onCreateEngine()
    {
        return new GLEngine();
    }

    public class GLEngine extends Engine
    {
        public final static int RENDERMODE_WHEN_DIRTY = 0;

        public final static int RENDERMODE_CONTINUOUSLY = 1;

        private GLThread glThread;

        private EGLConfigChooser eglConfigChooser;

        private EGLContextFactory eglContextFactory;

        private EGLWindowSurfaceFactory eglWindowSurfaceFactory;

        private GLWrapper glWrapper;

        private int debugFlags;

        public GLEngine()
        {
            super();
        }

        @Override
        public void onVisibilityChanged(boolean visible)
        {
            if (visible)
            {
                onResume();
            }
            else
            {
                onPause();
            }
            super.onVisibilityChanged(visible);
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder)
        {
            super.onCreate(surfaceHolder);
            // Log.d(TAG, "GLEngine.onCreate()");
        }

        @Override
        public void onDestroy()
        {
            super.onDestroy();
            // Log.d(TAG, "GLEngine.onDestroy()");
            glThread.requestExitAndWait();
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height)
        {
            // Log.d(TAG, "onSurfaceChanged()");
            glThread.onWindowResize(width, height);
            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onSurfaceCreated(SurfaceHolder holder)
        {
            // Log.d(TAG, "onSurfaceCreated()");
            glThread.surfaceCreated(holder);
            super.onSurfaceCreated(holder);
        }

        @Override
        public void onSurfaceDestroyed(SurfaceHolder holder)
        {
            // Log.d(TAG, "onSurfaceDestroyed()");
            glThread.surfaceDestroyed();
            super.onSurfaceDestroyed(holder);
        }

        public void setGLWrapper(GLWrapper glWrapper)
        {
            this.glWrapper = glWrapper;
        }

        public void setDebugFlags(int debugFlags)
        {
            this.debugFlags = debugFlags;
        }

        public int getDebugFlags()
        {
            return debugFlags;
        }

        public void setRenderer(Renderer renderer)
        {
            checkRenderThreadState();
            if (eglConfigChooser == null)
            {
                eglConfigChooser = new SimpleEGLConfigChooser(true);
            }
            if (eglContextFactory == null)
            {
                eglContextFactory = new DefaultContextFactory();
            }
            if (eglWindowSurfaceFactory == null)
            {
                eglWindowSurfaceFactory = new DefaultWindowSurfaceFactory();
            }
            glThread = new GLThread(renderer, eglConfigChooser, eglContextFactory, eglWindowSurfaceFactory, glWrapper);
            glThread.start();
        }

        public void setEGLContextFactory(EGLContextFactory factory)
        {
            checkRenderThreadState();
            eglContextFactory = factory;
        }

        public void setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory factory)
        {
            checkRenderThreadState();
            eglWindowSurfaceFactory = factory;
        }

        public void setEGLConfigChooser(EGLConfigChooser configChooser)
        {
            checkRenderThreadState();
            eglConfigChooser = configChooser;
        }

        public void setEGLConfigChooser(boolean needDepth)
        {
            setEGLConfigChooser(new SimpleEGLConfigChooser(needDepth));
        }

        public void setEGLConfigChooser(int redSize, int greenSize, int blueSize, int alphaSize, int depthSize,
            int stencilSize)
        {
            setEGLConfigChooser(new ComponentSizeChooser(redSize, greenSize, blueSize, alphaSize, depthSize,
                stencilSize));
        }

        public void setRenderMode(int renderMode)
        {
            glThread.setRenderMode(renderMode);
        }

        public int getRenderMode()
        {
            return glThread.getRenderMode();
        }

        public void requestRender()
        {
            glThread.requestRender();
        }

        public void onPause()
        {
            glThread.onPause();
        }

        public void onResume()
        {
            glThread.onResume();
        }

        public void queueEvent(Runnable r)
        {
            glThread.queueEvent(r);
        }

        private void checkRenderThreadState()
        {
            if (glThread != null)
            {
                throw new IllegalStateException("setRenderer has already been called for this instance.");
            }
        }
    }

    public interface Renderer
    {
        public void onSurfaceCreated(GL10 gl, EGLConfig config);

        public void onSurfaceChanged(GL10 gl, int width, int height);

        public void onDrawFrame(GL10 gl);
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

import javax.microedition.khronos.egl.*;

interface EGLConfigChooser
{
    EGLConfig chooseConfig(EGL10 egl, EGLDisplay display);
}

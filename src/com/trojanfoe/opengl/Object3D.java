/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

import javax.microedition.khronos.opengles.*;

public class Object3D
{
    protected Object mLock;
    protected Model mModel;
    protected Vector3f mPosition;
    protected Vector3f mRotation;
    protected float mScale;
    protected int mDrawMode;

    public Object3D()
    {
        mLock = new Object();
        mModel = null;
        mPosition = new Vector3f();
        mRotation = new Vector3f();
        mScale = 1.0f;
        mDrawMode = Model.DRAW_FILLED;
    }

    public Object3D(Model model)
    {
        super();
        mModel = model;
    }

    /**
     * Draw the setModel with the specified transformations.
     * 
     * @param gl The GL object.
     */
    public void draw(GL10 gl)
    {
        if (mModel == null)
        {
            return;
        }

        synchronized (mLock)
        {
            gl.glPushMatrix();

            if (!mPosition.isEmpty())
            {
                gl.glTranslatef(mPosition.get(0), mPosition.get(1), mPosition.get(2));
            }

            if (mScale != 1.0f)
            {
                gl.glScalef(mScale, mScale, mScale);
            }

            if (mRotation.get(0) != 0.0f)
            {
                gl.glRotatef(mRotation.get(0), 1.0f, 0.0f, 0.0f);
            }
            if (mRotation.get(1) != 0.0f)
            {
                gl.glRotatef(mRotation.get(1), 0.0f, 1.0f, 0.0f);
            }
            if (mRotation.get(2) != 0.0f)
            {
                gl.glRotatef(mRotation.get(2), 0.0f, 0.0f, 1.0f);
            }

            mModel.draw(gl, mDrawMode);

            gl.glPopMatrix();

            gl.glFlush();
        }
    }

    public void setModel(Model model)
    {
        synchronized (mLock)
        {
            mModel = model;
            if (mModel != null)
            {
                // Force the setModel to reset the vertex & normal buffers
                mModel.resetInitted();
            }
        }
    }

    public boolean setModel(String modelName)
    {
        synchronized (mLock)
        {
            if (mModel != null && mModel.getName().equals(modelName))
            {
                return true; // Already loaded this setModel!
            }
            setModel(Model.loadModel(modelName));
            return mModel != null;
        }
    }

    public Model getModel()
    {
        return mModel;
    }

    public void setPosition(Vector3f position)
    {
        mPosition = position;
    }

    public Vector3f getPosition()
    {
        return mPosition;
    }

    public void setRotation(Vector3f rotation)
    {
        mRotation = rotation;
    }

    public Vector3f getRotation()
    {
        return mRotation;
    }

    public void setScale(float scale)
    {
        mScale = scale;
    }

    public float getScale()
    {
        return mScale;
    }

    public void setDrawMode(int drawMode)
    {
        if (drawMode < Model.DRAW_FILLED || drawMode > Model.DRAW_WIREFRAME_HIDDEN_LINE)
        {
            drawMode = Model.DRAW_FILLED;
        }
        mDrawMode = drawMode;
    }

    public int getDrawMode()
    {
        return mDrawMode;
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

public class Face
{
    protected int[] mVertices;

    protected int[] mNormals;

    protected Material mMaterial;

    public Face(int v1, int v2, int v3, Material material)
    {
        mVertices = new int[3];
        mVertices[0] = v1;
        mVertices[1] = v2;
        mVertices[2] = v3;
        this.mMaterial = material;
    }

    public Face(int v1, int v2, int v3, int n1, int n2, int n3, Material material)
    {
        mVertices = new int[3];
        mVertices[0] = v1;
        mVertices[1] = v2;
        mVertices[2] = v3;
        mNormals = new int[3];
        mNormals[0] = n1;
        mNormals[1] = n2;
        mNormals[2] = n3;
        this.mMaterial = material;
    }

    public int[] vertices()
    {
        return mVertices;
    }

    public int vertex(int index)
    {
        return mVertices[index];
    }

    public int normal(int index)
    {
        // If mNormals have not been defined during conversion
        // from Blender then we assume that the calculated normal
        // uses the same index as the vertex...
        return (mNormals != null) ? mNormals[index] : mVertices[index];
    }

    public Material material()
    {
        return mMaterial;
    }

    public boolean usesVertex(int v)
    {
        return mVertices != null && (mVertices[0] == v || mVertices[1] == v || mVertices[2] == v);
    }
}

/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

import javax.microedition.khronos.egl.*;

/**
 * An interface for customizing the eglCreateWindowSurface and eglDestroySurface calls.
 * <p/>
 * <p/>
 * This interface must be implemented by clients wishing to call
 * {@link GLWallpaperService#setEGLWindowSurfaceFactory(EGLWindowSurfaceFactory)}
 */
interface EGLWindowSurfaceFactory
{
    EGLSurface createWindowSurface(EGL10 egl, EGLDisplay display, EGLConfig config, Object nativeWindow);

    void destroySurface(EGL10 egl, EGLDisplay display, EGLSurface surface);
}

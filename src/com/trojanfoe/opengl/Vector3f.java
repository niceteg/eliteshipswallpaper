/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

public class Vector3f
{
    protected float[] mFloats;

    public Vector3f()
    {
        this(0.0f, 0.0f, 0.0f);
    }

    public Vector3f(float x, float y, float z)
    {
        mFloats = new float[3];
        mFloats[0] = x;
        mFloats[1] = y;
        mFloats[2] = z;
    }

    public Vector3f(Vector3f other)
    {
        this(other.mFloats[0], other.mFloats[1], other.mFloats[2]);
    }

    public void copy(Vector3f other)
    {
        mFloats[0] = other.mFloats[0];
        mFloats[1] = other.mFloats[1];
        mFloats[2] = other.mFloats[2];
    }

    public void copyTo(float[] array, int index)
    {
        array[index++] = mFloats[0];
        array[index++] = mFloats[1];
        array[index++] = mFloats[2];
    }

    public void reset()
    {
        mFloats[0] = 0.0f;
        mFloats[1] = 0.0f;
        mFloats[2] = 0.0f;
    }

    public float[] getArray()
    {
        return mFloats;
    }

    public float get(int index)
    {
        return mFloats[index];
    }

    public void set(int index, float value)
    {
        mFloats[index] = value;
    }

    public void add(Vector3f amount)
    {
        mFloats[0] += amount.mFloats[0];
        mFloats[1] += amount.mFloats[1];
        mFloats[2] += amount.mFloats[2];
    }

    public void sub(Vector3f amount)
    {
        mFloats[0] -= amount.mFloats[0];
        mFloats[1] -= amount.mFloats[1];
        mFloats[2] -= amount.mFloats[2];
    }

    public void add(Vector3f amount, float clamp)
    {
        mFloats[0] = (mFloats[0] + amount.mFloats[0]) % clamp;
        mFloats[1] = (mFloats[1] + amount.mFloats[1]) % clamp;
        mFloats[2] = (mFloats[2] + amount.mFloats[2]) % clamp;
    }

    public void sub(Vector3f amount, float clamp)
    {
        mFloats[0] = (mFloats[0] - amount.mFloats[0]) % clamp;
        mFloats[1] = (mFloats[1] - amount.mFloats[1]) % clamp;
        mFloats[2] = (mFloats[2] - amount.mFloats[2]) % clamp;
    }

    public void mul(float d)
    {
        mFloats[0] *= d;
        mFloats[1] *= d;
        mFloats[2] *= d;
    }

    public void div(float d)
    {
        mFloats[0] /= d;
        mFloats[1] /= d;
        mFloats[2] /= d;
    }

    public Vector3f diff(Vector3f other)
    {
        Vector3f diff = new Vector3f();
        diff.mFloats[0] = mFloats[0] - other.mFloats[0];
        diff.mFloats[1] = mFloats[1] - other.mFloats[1];
        diff.mFloats[2] = mFloats[2] - other.mFloats[2];
        return diff;
    }

    public Vector3f crossProduct(Vector3f other)
    {
        Vector3f product = new Vector3f();
        product.mFloats[0] = (mFloats[1] * other.mFloats[2]) - (mFloats[2] * other.mFloats[1]);
        product.mFloats[1] = (mFloats[2] * other.mFloats[0]) - (mFloats[0] * other.mFloats[2]);
        product.mFloats[2] = (mFloats[0] * other.mFloats[1]) - (mFloats[1] * other.mFloats[0]);
        return product;
    }

    public void normalize()
    {
        float len = (float) (Math.sqrt((mFloats[0] * mFloats[0]) + (mFloats[1] * mFloats[1])
            + (mFloats[2] * mFloats[2])));
        if (len == 0.0f)
        {
            len = 1.0f;
        }
        else if (len < 0.0f)
        {
            len = -len;
        }
        div(len);
    }

    public boolean isEmpty()
    {
        return mFloats == null || (mFloats[0] == 0.0f && mFloats[1] == 0.0f && mFloats[2] == 0.0f);
    }

    @Override
    public String toString()
    {
        if (mFloats != null)
        {
            return "[0]=" + mFloats[0] + ", [1]=" + mFloats[1] + ", [2]=" + mFloats[2];
        }
        return "null";
    }
}

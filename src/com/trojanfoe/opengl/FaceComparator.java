/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.opengl;

import java.util.Comparator;

public class FaceComparator implements Comparator<Face>
{
    public int compare(Face face1, Face face2)
    {
        // Use the getName of the mMaterial for the comparison
        return face1.mMaterial.getName().compareTo(face2.mMaterial.getName());
    }

    public boolean equals(Object other)
    {
        return other != null && other instanceof FaceComparator && this == other;
    }
}

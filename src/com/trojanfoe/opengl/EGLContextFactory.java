/*
 * Copyright (c) 2011 trojanfoe apps
 */

package com.trojanfoe.opengl;

import javax.microedition.khronos.egl.*;

/**
 * An interface for customizing the eglCreateContext and eglDestroyContext calls.
 * <p/>
 * <p/>
 * This interface must be implemented by clients wishing to call
 * {@link GLWallpaperService#setEGLContextFactory(EGLContextFactory)}
 */
interface EGLContextFactory
{
    EGLContext createContext(EGL10 egl, EGLDisplay display, EGLConfig eglConfig);

    void destroyContext(EGL10 egl, EGLDisplay display, EGLContext context);
}

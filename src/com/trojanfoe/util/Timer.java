/*
 * Copyright (c) 2011 trojanfoe apps
 */
package com.trojanfoe.util;

import android.os.*;

/**
 * Timer - collect timing statistics.
 */
public class Timer
{
    protected long mMinTime, mMaxTime, mAvgTime, mCumulTime;

    protected int mCounter, mTotalCounter;

    // 0: <5ms / 1: <10ms / 2: <15ms / 3= <20ms / 4=>20ms
    protected int[] mTimerange = new int[5];

    // Last operation start time
    protected long mLastStartTime;

    public Timer()
    {
        mMinTime = 999999;
        mMaxTime = mAvgTime = mCumulTime = 0L;
        mCounter = mTotalCounter = 0;
        for (int i = 0; i < 5; i++)
        {
            mTimerange[i] = 0;
        }
        mLastStartTime = 0L;
    }

    public long getAvgTime()
    {
        return mAvgTime;
    }

    public long getMaxTime()
    {
        return mMaxTime;
    }

    public long getMinTime()
    {
        return mMinTime;
    }

    public int getTimerange(int index)
    {
        if (index >= 0 && index < 5)
        {
            return mTimerange[index];
        }
        return 0;
    }

    public int getCounter()
    {
        return mCounter;
    }

    public int getTotalCounter()
    {
        return mTotalCounter;
    }

    /**
     * Mark the start of a single operation.
     */
    public void startOperation()
    {
        mLastStartTime = SystemClock.elapsedRealtime();
    }

    /**
     * Mark the end of a single operation. This method will update the mMinTime, mMaxTime and
     * mAvgTime fields and possibly reset the mMinTime and mMaxTime fields.
     */
    public void endOperation()
    {
        long elapsed = SystemClock.elapsedRealtime() - mLastStartTime;
        if (elapsed < mMinTime)
        {
            mMinTime = elapsed;
        }
        if (elapsed > mMaxTime)
        {
            mMaxTime = elapsed;
        }
        mCumulTime += elapsed;

        if (elapsed <= 5)
        {
            mTimerange[0]++;
        }
        else if (elapsed <= 10)
        {
            mTimerange[1]++;
        }
        else if (elapsed <= 15)
        {
            mTimerange[2]++;
        }
        else if (elapsed <= 20)
        {
            mTimerange[3]++;
        }
        else
        {
            mTimerange[4]++;
        }

        if (++mCounter >= 100)
        {
            mAvgTime = mCumulTime / mCounter;
            mCumulTime = 0L;
            mCounter = 0;
        }

        if ((++mTotalCounter % 5000) == 0)
        {
            // Reset min/max values
            mMinTime = 999999;
            mMaxTime = 0;
        }
    }

    /**
     * @return The formatted timing stats.
     */
    @Override
    public String toString()
    {
        return "min=" + mMinTime + " max=" + mMaxTime + " avg=" + mAvgTime + " 1-5=" + mTimerange[0] + " 6-10="
            + mTimerange[1] + " 11-15=" + mTimerange[2] + " 16-20=" + mTimerange[3] + " 20+=" + mTimerange[4];
    }
}
